extends Camera

export (NodePath) onready var player = get_node(player)
export (NodePath) onready var pool = get_node(pool)
export (float) var quality
onready var camera = player.get_node("Camera")

func _physics_process(_delta):
	
	# Viewport size should be the same ration as the mirror. In this case,
	# we are making it so that quality is equal to the number of pixels per
	# length unity of each side.
	get_parent().size = pool.mesh.size * quality
	
	var cam_pos: Vector3 = camera.global_transform.origin
	var pool_pos: Vector3 = pool.global_transform.origin
	var normal: Vector3 = Vector3.UP # This should be the direction of your mirror, normalized.
	
	# Mirrored camera's position. Should work for all cases, as long as
	# your normal and position vectors are correctly updated.
	self.translation = cam_pos - 2 * normal * normal.dot(cam_pos - pool_pos)
	
	# Camera size value is the z-near's size in space units. Being more accurate, if the
	# camera is set to keep width, size is z-near's width. If set to keep height, it's its
	# height. The other coordinate is set by the viewport's size in a way that the z-near
	# plane and the viewport are always the same ratio.
	self.size = pool.mesh.size.x
	
	# Computing frustum offset. First, let's understand what this vector really is.
	# Let's say you have your camera set to frustum mode, and the z-near plane's center's
	# position is, in the camera's local coordinates, some vector (x,y,z). Then, the
	# frustum offset is equal to (x,y) and the near value is equal to -z. So you know that
	# the camera is always looking at the -z direction and z-near plane is perpendicular
	# to the z-axis. That's all the information we need. We rotate our camera to make it
	# look up on the editor, take our mirror/lake's position, transform it to the camera's
	# local basis and we have our offset and near values. The far value can be any large
	# arbitrary number.
	var local_pool_pos = global_transform.xform_inv(pool_pos)
	self.near = -local_pool_pos.z
	self.frustum_offset = Vector2(local_pool_pos.x, local_pool_pos.y)


